﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication8.Parsers
{
    public class NetworkDialogEventParser : IParser
    {        
        string iOS_networkDialog_On = "Network failure modal presented.";
        string iOS_networkDialog_Off = "Network failure modal dismissed.";
        string android_networkDialog_On = "Will display network outage message.";
        string android_networkDialog_Off = "Will dismiss network resilience dialog.";
        
        public class Report
        {
            public DateTime time1;
            public DateTime time2;
            public TimeSpan span;
            public bool isAndroid;
        }

        List<Report> metrics = new List<Report>();

        string file;
        string outputFile;

        public bool setup(string input, string output)
        {
            input = file;
            outputFile = output;
            
            return true;
        }        

        bool IParser.computeResults(int flag)
        {
            //logData("Computing stats...");

            double mean = 0.0;
            double min = 0.0;
            double max = 0.0;

            foreach (Report r in metrics)
            {
                mean += r.span.TotalSeconds;

                if (min == 0)
                {
                    min = r.span.TotalSeconds;
                }
                else if (r.span.TotalSeconds < min)
                {
                    min = r.span.TotalSeconds;
                }

                if (r.span.TotalSeconds > max)
                    max = r.span.TotalSeconds;

                string output = string.Format("{0},{1},{2},{3}", (r.isAndroid) ? "A" : "I", r.time1.ToLongTimeString(), r.time2.ToLongTimeString(), r.span.TotalSeconds);
                System.IO.File.AppendAllText("output.csv", output + Environment.NewLine);
            }


            mean /= metrics.Count;

            //logData("Total Files Scanned: " + filesScanned.ToString());
            //logData("Total Events Mined: " + metrics.Count);
            //logData("Mean Duration " + mean.ToString() + " Min " + min.ToString() + " Max " + max.ToString());
            
            var android = metrics.Where(eachMetric => eachMetric.isAndroid == true);
            var iOS = metrics.Where(eachMetric => eachMetric.isAndroid == false);

            int androidCount = 0;
            foreach (var a in android)
            {
                androidCount++;
            }
            //logData("Android " + androidCount);
            
            int iOSCount = 0;
            foreach (var a in iOS)
            {
                iOSCount++;
            }
            //logData("iOS " + iOSCount);

            return true;
        }

        DateTime getDate(string LogEntry)
        {
            DateTime result = new DateTime();
            if (LogEntry.Contains(iOS_networkDialog_Off) || LogEntry.Contains(iOS_networkDialog_On)) //TODO find more generic way of getting this!
            {
                string[] split = LogEntry.Split(' ');
                string[] calendarSplit = split[0].Split('-');
                string[] timeSplit = split[1].Split(':');
                string[] secondSplit = timeSplit[2].Split('.');
                result = new DateTime(Convert.ToInt32(calendarSplit[0]), Convert.ToInt32(calendarSplit[1]), Convert.ToInt32(calendarSplit[2]), Convert.ToInt32(timeSplit[0]), Convert.ToInt32(timeSplit[1]), Convert.ToInt32(secondSplit[0]), Convert.ToInt32(secondSplit[1]));
            }
            else
            {
                //<Info dateTime="2017-05-06 
                string[] test = LogEntry.Split('\"');
                string[] categories = test[1].Split(' ');
                string date = categories[0];
                string time = categories[1];
                string[] dateParts = date.Split('-'); //yr-m-d
                string[] delta = time.Split(':');     // h-m-s.ms

                result = new DateTime(Convert.ToInt32(dateParts[0]), Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[2]), Convert.ToInt32(delta[0]), Convert.ToInt32(delta[1]), Convert.ToInt32(delta[2].Split('.')[0]), Convert.ToInt32(delta[2].Split('.')[1]));
            }

            return result;
        }

        public bool analyze(int lineIndex)
        {
            throw new NotImplementedException();
        }
    }
}
