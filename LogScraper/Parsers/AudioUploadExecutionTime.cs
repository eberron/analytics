﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication8.Parsers
{
    public class AudioUploadExecutionTime : IParser
    {
        string executionTime_ios = "executionTime [";
        string audioSend_ios = "/audio/send";

        string[] allLines;
        string outputPath;

        string inputFile;
        string outputFile;

        class Sample
        {
            public string file;
            public DateTime timestamp;
            public string executionTime;
        }

        List<Sample> outputData = new List<Sample>();

        bool IParser.setup(string inputFilePath, string outputFilePath)
        {
            this.inputFile = inputFilePath;
            this.outputFile = outputFilePath;
            this.allLines = System.IO.File.ReadAllLines(inputFilePath);
            return true;
        }

        bool IParser.analyze(int lineIndex)
        {
            string curLine = allLines[lineIndex];
            //Console.WriteLine(lineIndex);
            if (curLine.Contains(audioSend_ios))
            {
                //Console.WriteLine("New sample");
                Sample s = new Sample();
                s.file = inputFile;
                s.timestamp = getDate(allLines[lineIndex]);
                //Console.WriteLine("date: " + s.timestamp);
                s.executionTime = parseExecutionTime(allLines[lineIndex]);
                //Console.WriteLine("execution: " + s.executionTime);
                outputData.Add(s);
            }
            
            return true;
        }
       
        DateTime getDate(string line)
        {
            string[] split = line.Split(' ');
            string[] calendarSplit = split[0].Split('-');
            string[] timeSplit = split[1].Split(':');
            string[] secondSplit = timeSplit[2].Split('.');
            return new DateTime(Convert.ToInt32(calendarSplit[0]), Convert.ToInt32(calendarSplit[1]), Convert.ToInt32(calendarSplit[2]), Convert.ToInt32(timeSplit[0]), Convert.ToInt32(timeSplit[1]), Convert.ToInt32(secondSplit[0]), Convert.ToInt32(secondSplit[1]));
        }

        //string sample = "executionTime [0.041656]  id [2982] resource";
        string parseExecutionTime(string line)
        {
            string executionTime = null;
            
            int index = line.IndexOf("executionTime [");
            index = index + ("executionTime [".Length);
            string withBrackets = line.Substring(index).Split(' ')[0];
            executionTime = withBrackets.Substring(0, withBrackets.Length - 2);
            Console.WriteLine(executionTime);
            return executionTime;
        }

        public bool computeResults(int flag)
        {
            Console.WriteLine("Count: " + outputData.Count);
            foreach (Sample r in this.outputData)
            {
                string output = string.Format("{0},{1},{2}", inputFile, r.timestamp.ToLongTimeString(), r.executionTime);
                System.IO.File.AppendAllText(this.outputFile, output + Environment.NewLine);
            }
            return true;
        }
    }


}
