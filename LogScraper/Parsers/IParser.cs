﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication8
{
    public interface IParser
    {
        bool setup(string inputFilePath, string outputFilePath);
        bool analyze(int lineIndex);
        bool computeResults(int flag);        
    }
}
