﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Linq;
using WindowsFormsApplication8.Parsers;

namespace WindowsFormsApplication8
{
    public partial class Form1 : Form
    {
        int filesScanned = 0;
        
            
        public Form1()
        {
            InitializeComponent();
        }

        IParser CurrentParser = null;
        
        private void button1_Click(object sender, EventArgs e)
        {
            CurrentParser = new AudioUploadExecutionTime();
            
            Task.Run(() =>
            {                
                DateTime start = DateTime.Now;
                try
                {
                    string dir = System.IO.Directory.GetCurrentDirectory();
                    string[] files = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory(), "*.log", System.IO.SearchOption.AllDirectories);

                    filesScanned = files.Length;
                    logData("File count: " + filesScanned.ToString());
                    
                    for (int c=0;c<filesScanned;c++)
                    {
                        CurrentParser.setup(files[c], "output.csv");                        
                        logData("Scanning file " + (c+1) + " out of "+filesScanned);
                        
                        int length = System.IO.File.ReadAllLines(files[c]).Length;
                        for(int i=0;i<length;i++)
                        {
                            CurrentParser.analyze(i);
                        }                                               
                    }                    
                }
                catch (Exception){}
                                                
                CurrentParser.computeResults(0);
                logData("Analysis Duration: " + (DateTime.Now - start).ToString());
            });
        }
        
        DateTime[] getDates(string[] logEntries)
        {
            DateTime[] result = new DateTime[logEntries.Length];

            for (int i = 0; i <= logEntries.Length; i++)
            {
                result[i] = getDate(logEntries[i]);
            }

            return result;
        }

        DateTime getDate(string logEntry)
        {            
            string[] split = logEntry.Split(' ');
            string[] calendarSplit = split[0].Split('-');
            string[] timeSplit = split[1].Split(':');
            string[] secondSplit = timeSplit[2].Split('.');
            return new DateTime(Convert.ToInt32(calendarSplit[0]), Convert.ToInt32(calendarSplit[1]), Convert.ToInt32(calendarSplit[2]), Convert.ToInt32(timeSplit[0]), Convert.ToInt32(timeSplit[1]), Convert.ToInt32(secondSplit[0]), Convert.ToInt32(secondSplit[1]));            
        }

        //Needs more generic way of finding out platform! 
        // maybe looking at the meta data in log file?
        /*
        DateTime getDate(string LogEntry)
        {
            DateTime result = new DateTime();
            if (LogEntry.Contains(iOS_networkDialog_Off) || LogEntry.Contains(iOS_networkDialog_On)) //TODO find more generic way of getting this!
            {
                string[] split = LogEntry.Split(' ');
                string[] calendarSplit = split[0].Split('-');
                string[] timeSplit = split[1].Split(':');
                string[] secondSplit = timeSplit[2].Split('.');
                result = new DateTime(Convert.ToInt32(calendarSplit[0]), Convert.ToInt32(calendarSplit[1]), Convert.ToInt32(calendarSplit[2]), Convert.ToInt32(timeSplit[0]), Convert.ToInt32(timeSplit[1]), Convert.ToInt32(secondSplit[0]), Convert.ToInt32(secondSplit[1]));
            }
            else
            {
                //<Info dateTime="2017-05-06 
                string[] test = LogEntry.Split('\"');
                string[] categories = test[1].Split(' ');
                string date = categories[0];
                string time = categories[1];
                string[] dateParts = date.Split('-'); //yr-m-d
                string[] delta = time.Split(':');     // h-m-s.ms

                result = new DateTime(Convert.ToInt32(dateParts[0]), Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[2]), Convert.ToInt32(delta[0]), Convert.ToInt32(delta[1]), Convert.ToInt32(delta[2].Split('.')[0]), Convert.ToInt32(delta[2].Split('.')[1]));
            }

            return result;
        }
        */

        private void logData(string msg)
        {
            try
            {
                textBox1.BeginInvoke((MethodInvoker)delegate ()
                   {
                       textBox1.AppendText(msg + Environment.NewLine);
                       textBox1.ScrollToCaret();
                   });
                System.IO.File.AppendAllText("log.txt", DateTime.Now + " >>   " + msg + Environment.NewLine);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);                
            }
        }
    }
}
